﻿using System.Collections.Generic;

namespace WebApplication.WEB.Models
{
    public class QuestionnaireViewModel
    {
        public string FName { get; set; }
        public string LName { get; set; }
        public string Sex { get; set; }
        public int Mark { get; set; }

        public List<NetworkViewModel> Networks { get; set; }

        /// <summary>
        /// If the user agrees to the processing of personal data.
        /// </summary>
        public bool IsConfirmed { get; set; }

        public QuestionnaireViewModel()
        {
            Networks = new List<NetworkViewModel>
            {
                new NetworkViewModel {IsSelected = false, Name = "Telegram"},
                new NetworkViewModel {IsSelected = false, Name = "Facebook"},
                new NetworkViewModel {IsSelected = false, Name = "Viber"},
                new NetworkViewModel {IsSelected = false, Name = "Whatsapp"},
                new NetworkViewModel {IsSelected = false, Name = "Instagram"}
            };
        }

    }
}