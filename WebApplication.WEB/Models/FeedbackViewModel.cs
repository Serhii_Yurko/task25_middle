﻿using System.Collections.Generic;
using WebApplication.BLL.DTO;

namespace WebApplication.WEB.Models
{
    public class FeedbackViewModel
    {
        public List<FeedbackDTO> Feedback { get; set; }

        public FeedbackDTO FeedbackObject { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}
