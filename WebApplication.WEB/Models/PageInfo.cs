﻿using System;

namespace WebApplication.WEB.Models
{
    /// <summary>
    /// Model for pagination pages.
    /// </summary>
    public class PageInfo
    {
        /// <summary>
        /// Current page number.
        /// </summary>
        public int PageNumber { get; set; } 
        
        /// <summary>
        /// Object count on the page.
        /// </summary>
        public int PageSize { get; set; } 
        
        /// <summary>
        /// Total object count.
        /// </summary>
        public int TotalItems { get; set; } 
       
        /// <summary>
        /// Total page count.
        /// </summary>
        public int TotalPages => (int)Math.Ceiling((decimal)TotalItems / PageSize);
    }
}
