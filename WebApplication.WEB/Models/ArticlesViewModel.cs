﻿using System.Collections.Generic;
using System.Web.Mvc;
using WebApplication.BLL.DTO;

namespace WebApplication.WEB.Models
{
    public class ArticlesViewModel
    {
        public List<ArticleDTO> Articles { get; set; }
        public PageInfo PageInfo { get; set; }
        public ArticlesViewModel()
        {
            Articles = new List<ArticleDTO>();
        }
    }
}
