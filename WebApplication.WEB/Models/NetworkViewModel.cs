﻿namespace WebApplication.WEB.Models
{
    /// <summary>
    /// Network where notifications are sent.
    /// </summary>
    public class NetworkViewModel
    {
        public string Name { get; set; }
        /// <summary>
        /// If the user select network with such Name.
        /// </summary>
        public bool IsSelected { get; set; }
    }

}