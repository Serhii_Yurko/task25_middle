﻿using System.Collections.Generic;
using System.Web.Mvc;
using WebApplication.BLL.DTO;

namespace WebApplication.WEB.Models
{
    public class ArticleTagsViewModel
    {
        public ArticleDTO Article { get; set; }
        public List<int> SelectedTags { get; set; }
        public List<SelectListItem> Tags { get; set; }
        public string InputTag { get; set; }
    }
}
