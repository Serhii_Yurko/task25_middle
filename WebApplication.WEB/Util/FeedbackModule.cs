﻿using Ninject.Modules;
using WebApplication.BLL.Interfaces;
using WebApplication.BLL.Services;

namespace WebApplication.WEB.Util
{
    /// <summary>
    /// Dependency injection for <c>FeedbackService</c> class.
    /// </summary>
    public class FeedbackModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IFeedbackService>().To<FeedbackService>();
        }
    }
}
