﻿using Ninject.Modules;
using WebApplication.BLL.Interfaces;
using WebApplication.BLL.Services;

namespace WebApplication.WEB.Util
{
    /// <summary>
    /// Dependency injection for <c>TagService</c> class.
    /// </summary>
    public class TagModule: NinjectModule
    {
        public override void Load()
        {
            Bind<ITagService>().To<TagService>();
        }
    }
}
