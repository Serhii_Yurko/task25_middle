﻿using Ninject.Modules;
using WebApplication.BLL.Interfaces;
using WebApplication.BLL.Services;

namespace WebApplication.WEB.Util
{
    /// <summary>
    /// Dependency injection for <c>QuestionnaireService</c> class.
    /// </summary>
    public class QuestionnaireModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IQuestionnaireService>().To<QuestionnaireService>();
        }
    }
}