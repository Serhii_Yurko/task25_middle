﻿using Ninject.Modules;
using WebApplication.BLL.Interfaces;
using WebApplication.BLL.Services;

namespace WebApplication.WEB.Util
{
    /// <summary>
    /// Dependency injection for <c>ArticleService</c> class.
    /// </summary>
    public class ArticleModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IArticleService>().To<ArticleService>();
        }
    }
}
