﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.BLL.DTO
{
    public class FeedbackDTO
    {
        public int Id { get; set; }
        /// <summary>
        /// User full name.
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "*Full name is required")]
        [Display(Name = "Full name")]
        public string Name { get; set; }
        /// <summary>
        /// Publication time.
        /// </summary>
        public DateTime Time { get; set; }
        /// <summary>
        /// The feedback text.
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "*Feedback is required")]
        [Display(Name = "Feedback")]
        public string Content { get; set; }
    }
}
