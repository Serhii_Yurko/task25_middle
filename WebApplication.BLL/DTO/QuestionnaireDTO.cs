﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebApplication.DAL.Entities;

namespace WebApplication.BLL.DTO
{
    public class QuestionnaireDTO
    {
        public int Id { get; set; }

        /// <summary>
        /// The user first name.
        /// </summary>
        public string FName { get; set; }

        /// <summary>
        /// The user last name.
        /// </summary>
        public string LName { get; set; }

        /// <summary>
        /// The user sex.
        /// </summary>
        public string Sex { get; set; }

        /// <summary>
        /// Networks for notification.
        /// </summary>
        public List<Network> Networks { get; set; }

        /// <summary>
        /// The mark of the site.
        /// </summary>
        [Required]
        public int Mark { get; set; }
    }
}
