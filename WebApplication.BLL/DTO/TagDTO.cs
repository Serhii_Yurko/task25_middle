﻿namespace WebApplication.BLL.DTO
{
    public class TagDTO
    {
        public int Id { get; set; }

        public string Text { get; set; }
    }
}
