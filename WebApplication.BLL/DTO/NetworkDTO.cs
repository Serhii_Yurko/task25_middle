﻿namespace WebApplication.BLL.DTO
{
    public class NetworkDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
