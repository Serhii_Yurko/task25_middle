﻿using System;
using System.Collections.Generic;

namespace WebApplication.BLL.DTO
{
    public class ArticleDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime Time { get; set; }
        public string Content { get; set; }
        public List<TagDTO> Tags { get; set; }
    }
}
