﻿using System.Collections.Generic;
using WebApplication.BLL.DTO;

namespace WebApplication.BLL.Interfaces
{
    public interface IArticleService
    {
        ArticleDTO GetArticle(int id);
        List<ArticleDTO> GetAllArticles();
        List<ArticleDTO> GetAllArticles(int id);
        void AddTags(int articleId, List<int> IdTags);
        void AddTag(int articleId, string tagText);
    }
}
