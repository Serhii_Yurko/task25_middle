﻿using System;
using System.Collections.Generic;
using WebApplication.BLL.DTO;

namespace WebApplication.BLL.Interfaces
{
    public interface IFeedbackService
    {
        List<FeedbackDTO> GetFeedbacks();
        void AddFeedback(FeedbackDTO feedback);
    }
}
