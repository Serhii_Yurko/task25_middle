﻿using System.Collections.Generic;
using WebApplication.BLL.DTO;

namespace WebApplication.BLL.Interfaces
{
    public interface ITagService
    {
        List<TagDTO> GetTags();
    }
}
