﻿using System.Collections.Generic;
using WebApplication.BLL.DTO;

namespace WebApplication.BLL.Interfaces
{
    public interface IQuestionnaireService
    {
        void AddQuestionnaire(string firstName, string lastName, string sex, int mark, List<NetworkDTO> network);
    }
}
