﻿using System.Collections.Generic;
using AutoMapper;
using WebApplication.BLL.DTO;
using WebApplication.BLL.Interfaces;
using WebApplication.DAL.Entities;
using WebApplication.DAL.Interfaces;

namespace WebApplication.BLL.Services
{
    /// <summary>
    /// Service that represents work with <c>Questionnaire</c> model. 
    /// </summary>
    public class QuestionnaireService : IQuestionnaireService
    {
        private readonly IUnitOfWork _database;

        public QuestionnaireService(IUnitOfWork unitOfWork)
        {
            _database = unitOfWork;
        }

        /// <summary>
        /// Add a new info about user to database.
        /// </summary>
        /// <param name="firstName">User first name.</param>
        /// <param name="lastName">User last name.</param>
        /// <param name="sex">User sex.</param>
        /// <param name="mark">The site mark from user.</param>
        /// <param name="network">Networks for notification.</param>
        public void AddQuestionnaire(string firstName, string lastName, string sex, int mark, List<NetworkDTO> network)
        {
            var config = new MapperConfiguration(cnfg => cnfg.CreateMap<NetworkDTO, Network>());
            var mapper = new Mapper(config);
            _database.Questionnaire.Create(new Questionnaire
            {
                FName = firstName,
                LName = lastName,
                Mark = mark,
                Sex = sex,
                Networks = mapper.Map<List<Network>>(network)
            });
            _database.Save();
        }
    }
}