﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using WebApplication.BLL.DTO;
using WebApplication.BLL.Interfaces;
using WebApplication.DAL.Entities;
using WebApplication.DAL.Interfaces;

namespace WebApplication.BLL.Services
{
    /// <summary>
    /// Service that represents work with <c>Article</c> model. 
    /// </summary>
    public class ArticleService : IArticleService
    {
        private readonly IUnitOfWork _database;

        public ArticleService(IUnitOfWork unitOfWork)
        {
            _database = unitOfWork;
        }

        /// <summary>
        /// Get an article from the database.
        /// </summary>
        /// <param name="id">A primary key that determines an article.</param>
        /// <returns>Found an article by id.</returns>
        public ArticleDTO GetArticle(int id)
        {
            var config = new MapperConfiguration(cnfg =>
            {
                cnfg.CreateMap<Article, ArticleDTO>();
                cnfg.CreateMap<Tag, TagDTO>();
            });
            var mapper = new Mapper(config);
            var article = _database.Articles.Get(id);
            return mapper.Map<ArticleDTO>(article);
        }

        /// <summary>
        /// Get all articles from database.
        /// </summary>
        /// <returns>All articles from the database.</returns>
        public List<ArticleDTO> GetAllArticles()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Article, ArticleDTO>();
                cfg.CreateMap<Tag, TagDTO>();
            });

            var mapper = new Mapper(config);
            var articles = _database.Articles.GetAll();
            return mapper.Map<IEnumerable<Article>, List<ArticleDTO>>(articles);
        }

        /// <summary>
        /// Get all articles with the same tag.
        /// </summary>
        /// <param name="id">Tag id for filtering articles.</param>
        /// <returns>Filtered list of articles by tag.</returns>
        public List<ArticleDTO> GetAllArticles(int id)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Article, ArticleDTO>();
                cfg.CreateMap<Tag, TagDTO>();
            });

            var mapper = new Mapper(config);
            var tag = _database.Tags.Get(id);
            var articles = tag.Articles.ToList();
            return mapper.Map<IEnumerable<Article>, List<ArticleDTO>>(articles);
        }

        /// <summary>
        /// Add tags to the article.
        /// </summary>
        /// <param name="articleId">The certain article specified by id.</param>
        /// <param name="IdTags">Tags for adding.</param>
        public void AddTags(int articleId, List<int> IdTags)
        {
            var article = _database.Articles.Get(articleId);
            if (IdTags != null)
            {
                foreach (var id in IdTags)
                {
                    if (article.Tags.All(item => item.Id != id))
                    {
                        var tag = _database.Tags.Get(id);
                        article.Tags.Add(tag);
                    }
                }
                _database.Articles.Update(article);
                _database.Save();
            }
        }

        /// <summary>
        /// Add tag to the article.
        /// </summary>
        /// <param name="articleId">The certain article specified by id.</param>
        /// <param name="tagText">Tag text for adding</param>
        public void AddTag(int articleId, string tagText)
        {
            var article = _database.Articles.Get(articleId);
            if (article.Tags.All(item => item.Text != tagText))
            {
                article.Tags.Add(new Tag { Text = tagText });
                _database.Articles.Update(article);
                _database.Save();
            }
        }
    }
}