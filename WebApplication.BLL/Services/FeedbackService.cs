﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using WebApplication.BLL.DTO;
using WebApplication.BLL.Interfaces;
using WebApplication.DAL.Entities;
using WebApplication.DAL.Interfaces;

namespace WebApplication.BLL.Services
{
    /// <summary>
    /// Service that represents work with feedback.
    /// </summary>
    public class FeedbackService: IFeedbackService
    {
        private readonly IUnitOfWork _database;

        public FeedbackService(IUnitOfWork unitOfWork)
        {
            _database = unitOfWork;
        }

        /// <summary>
        /// Add a new feedback to database.
        /// </summary>
        /// <param name="feedback">
        /// Feedback that should be added.
        /// </param>
        public void AddFeedback(FeedbackDTO feedback)
        {
            feedback.Time = DateTime.Now;
            var config = new MapperConfiguration(cfg => cfg.CreateMap<FeedbackDTO, Feedback>());
            var mapper = new Mapper(config);
            _database.Feedbacks.Create(mapper.Map<Feedback>(feedback));
            _database.Save();
        }

        /// <summary>
        /// Get all feedback from database.
        /// </summary>
        /// <returns>
        /// Feedback list. 
        /// </returns>
        public List<FeedbackDTO> GetFeedbacks()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Feedback, FeedbackDTO>());
            var mapper = new Mapper(config);
            var feedbacks = _database.Feedbacks.GetAll();
            return mapper.Map<IEnumerable<Feedback>, List<FeedbackDTO>>(feedbacks);
        }
    }
}