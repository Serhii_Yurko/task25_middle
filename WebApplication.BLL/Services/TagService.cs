﻿using System.Collections.Generic;
using AutoMapper;
using WebApplication.BLL.DTO;
using WebApplication.BLL.Interfaces;
using WebApplication.DAL.Entities;
using WebApplication.DAL.Interfaces;

namespace WebApplication.BLL.Services
{
    /// <summary>
    /// Service that represents work with <c>Tag</c> model. 
    /// </summary>
    public class TagService: ITagService
    {
        private readonly IUnitOfWork _database;

        public TagService(IUnitOfWork unitOfWork)
        {
            _database = unitOfWork;
        }

        /// <summary>
        /// Get all tags from database.
        /// </summary>
        /// <returns>All Tags.</returns>
        public List<TagDTO> GetTags()
        {
            var config = new MapperConfiguration(cnfg => cnfg.CreateMap<Tag, TagDTO>());
            var map = new Mapper(config);
            return map.Map<List<TagDTO>>(_database.Tags.GetAll());
        }
    }
}
