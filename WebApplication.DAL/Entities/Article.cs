﻿using System;
using System.Collections.Generic;

namespace WebApplication.DAL.Entities
{
    /// <summary>
    /// The model that represents an article and store in a database.
    /// </summary>
    public class Article
    { 
        public int Id { get; set; }
        public string Title { get; set; }
        
        /// <summary>
        /// Publication time.
        /// </summary>
        public DateTime Time { get; set; }
        public string Content { get; set; }

        /// <summary>
        /// Article has many tags.
        /// </summary>
        public virtual ICollection<Tag> Tags { get; set; }

        public Article()
        {
            Tags = new List<Tag>();
        }
    }
}