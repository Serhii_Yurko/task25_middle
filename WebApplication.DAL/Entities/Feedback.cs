﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.DAL.Entities
{
    /// <summary>
    /// The model that represents user feedback and store in a database.
    /// </summary>
    public class Feedback
    {
        public int Id { get; set; }
        /// <summary>
        /// User full name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Publication time.
        /// </summary>
        public DateTime Time { get; set; }
        /// <summary>
        /// The feedback text.
        /// </summary>
        public string Content { get; set; }
    }
}