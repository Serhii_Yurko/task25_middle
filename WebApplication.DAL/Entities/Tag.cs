﻿using System.Collections.Generic;

namespace WebApplication.DAL.Entities
{
    /// <summary>
    /// The model that represents an tag and store in a database.
    /// </summary>
    public class Tag
    {
        public int Id { get; set; }
        public string Text { get; set; }

        /// <summary>
        /// Tag has many articles.
        /// </summary>
        public virtual ICollection<Article> Articles { get; set; }

        public Tag()
        {
            Articles = new List<Article>();
        }
    }
}
