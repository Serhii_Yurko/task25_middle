﻿namespace WebApplication.DAL.Entities
{
    /// <summary>
    /// The model that represents an network for notification and store in a database.
    /// </summary>
    public class Network
    {
        public int Id { get; set; }
        
        /// <summary>
        /// Network name.
        /// </summary>
        public string Name { get; set; }
    }
}