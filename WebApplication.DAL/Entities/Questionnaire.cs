﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.DAL.Entities
{
    /// <summary>
    /// The model that represents an user questionnaire and store in a database.
    /// </summary>
    public class Questionnaire
    {
        public int Id { get; set; }
        
        /// <summary>
        /// The user first name.
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "*First name is required")]
        public string FName { get; set; }
        
        /// <summary>
        /// The user last name.
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "*Second name is required")]
        public string LName { get; set; }
        
        /// <summary>
        /// The user sex.
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "*Sex is required")]
        public string Sex { get; set; }
        
        /// <summary>
        /// Networks for notification.
        /// </summary>
        public List<Network>  Networks { get; set; }
        
        /// <summary>
        /// The mark of the site.
        /// </summary>
        [Required]
        public int Mark { get; set; }
    }
}