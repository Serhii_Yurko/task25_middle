﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WebApplication.DAL.EF;
using WebApplication.DAL.Entities;
using WebApplication.DAL.Interfaces;

namespace WebApplication.DAL.Repositories
{
    /// <summary>
    /// Implementation CRUD function for Article model.
    /// </summary>
    public class ArticleRepository : IRepository<Article>
    {
        private readonly LibraryContext _context;

        public ArticleRepository(LibraryContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Add new article to database.
        /// </summary>
        /// <param name="item">
        /// Article to add.
        /// </param>
        public void Create(Article item)
        {
            _context.Articles.Add(item);
        }

        /// <summary>
        /// Delete article from database.
        /// </summary>
        /// <param name="id">
        /// The value that identifies an article.
        /// </param>
        public void Delete(int id)
        {
            var article = _context.Articles.Find(id);
            if (article != null)
                _context.Articles.Remove(article);
        }

        /// <summary>
        /// Get articles with a certain condition. 
        /// </summary>
        /// <param name="predicate">
        /// Condition for filtering.
        /// </param>
        /// <returns>
        /// 
        /// </returns>
        public IEnumerable<Article> Find(Func<Article, bool> predicate)
        {
            return _context.Articles.Where(predicate).ToList();
        }

        public Article Get(int id)
        {
            return _context.Articles.Find(id);
        }

        public IEnumerable<Article> GetAll()
        {
            return _context.Articles.Include(a=>a.Tags).ToList();
        }

        public void Update(Article item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}