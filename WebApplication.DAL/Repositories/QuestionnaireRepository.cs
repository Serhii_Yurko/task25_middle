﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WebApplication.DAL.EF;
using WebApplication.DAL.Entities;
using WebApplication.DAL.Interfaces;

namespace WebApplication.DAL.Repositories
{
    /// <summary>
    /// Implementation CRUD function for Questionnaire model.
    /// </summary>
    public class QuestionnaireRepository : IRepository<Questionnaire>
    {
        private readonly LibraryContext _context;

        public QuestionnaireRepository(LibraryContext context)
        {
            _context = context;
        }

        public void Create(Questionnaire item)
        {
            _context.Questionnaires.Add(item);
        }

        public void Delete(int id)
        {
            var questionnaire = _context.Questionnaires.Find(id);
            if (questionnaire != null)
                _context.Questionnaires.Remove(questionnaire);
        }

        public IEnumerable<Questionnaire> Find(Func<Questionnaire, bool> predicate)
        {
            return _context.Questionnaires.Where(predicate).ToList();
        }

        public Questionnaire Get(int id)
        {
            return _context.Questionnaires.Find(id);
        }

        public IEnumerable<Questionnaire> GetAll()
        {
            return _context.Questionnaires;
        }

        public void Update(Questionnaire item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}