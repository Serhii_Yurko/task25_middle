﻿using System;
using WebApplication.DAL.EF;
using WebApplication.DAL.Entities;
using WebApplication.DAL.Interfaces;

namespace WebApplication.DAL.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly LibraryContext _context;
        private ArticleRepository _articleRepository;
        private TagRepository _tagRepository;
        private FeedbackRepository _feedbackRepository;
        private QuestionnaireRepository _questionnaireRepository;
        private NetworkRepository _networkRepository;
        public EFUnitOfWork()
        {
            _context = new LibraryContext();
        }

        public EFUnitOfWork(string connectionString)
        {
            _context = new LibraryContext(connectionString);
        }

        public IRepository<Article> Articles => _articleRepository ?? (_articleRepository = new ArticleRepository(_context));
        public IRepository<Feedback> Feedbacks => _feedbackRepository ?? (_feedbackRepository= new FeedbackRepository(_context));

        public IRepository<Questionnaire> Questionnaire =>
            _questionnaireRepository ?? (_questionnaireRepository = new QuestionnaireRepository(_context));

        public IRepository<Network> Networks =>
            _networkRepository ?? (_networkRepository = new NetworkRepository(_context));

        public IRepository<Tag> Tags => _tagRepository ?? (_tagRepository = new TagRepository(_context));

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
