﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WebApplication.DAL.EF;
using WebApplication.DAL.Entities;
using WebApplication.DAL.Interfaces;

namespace WebApplication.DAL.Repositories
{
    /// <summary>
    /// Implementation CRUD function for Feedback model.
    /// </summary>
    public class FeedbackRepository : IRepository<Feedback>
    {
        private readonly LibraryContext _context;
        public FeedbackRepository(LibraryContext context)
        {
            _context = context;
        }

        public void Create(Feedback item)
        {
            _context.Feedbacks.Add(item);
        }

        public void Delete(int id)
        {
            var feedback = _context.Feedbacks.Find(id);
            if (feedback != null)
                _context.Feedbacks.Remove(feedback);
        }

        public IEnumerable<Feedback> Find(Func<Feedback, bool> predicate)
        {
            return _context.Feedbacks.Where(predicate).ToList();
        }

        public Feedback Get(int id)
        {
            return _context.Feedbacks.Find(id);
        }

        public IEnumerable<Feedback> GetAll()
        {
            return _context.Feedbacks;
        }

        public void Update(Feedback item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}