﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WebApplication.DAL.EF;
using WebApplication.DAL.Entities;
using WebApplication.DAL.Interfaces;

namespace WebApplication.DAL.Repositories
{
    public class NetworkRepository: IRepository<Network>
    {
        private LibraryContext _context;
        public NetworkRepository(LibraryContext context)
        {
            _context = context;
        }

        public IEnumerable<Network> GetAll()
        {
            return _context.Networks;
        }

        public Network Get(int id)
        {
            return _context.Networks.Find(id);
        }

        public IEnumerable<Network> Find(Func<Network, bool> predicate)
        {
            return _context.Networks.Where(predicate).ToList();
        }

        public void Create(Network item)
        {
            _context.Networks.Add(item);
        }

        public void Update(Network item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var item = _context.Networks.Find(id);
            if (item != null)
                _context.Networks.Remove(item);
        }
    }
}
