﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WebApplication.DAL.EF;
using WebApplication.DAL.Entities;
using WebApplication.DAL.Interfaces;

namespace WebApplication.DAL.Repositories
{
    public class TagRepository: IRepository<Tag>
    {
        private readonly LibraryContext _context;
        public TagRepository(LibraryContext context)
        {
            _context = context;
        }
        public IEnumerable<Tag> GetAll()
        {
            return _context.Tags.ToList();
        }

        public Tag Get(int id)
        {
            return _context.Tags.Find(id);
        }

        public IEnumerable<Tag> Find(Func<Tag, bool> predicate)
        {
            return _context.Tags.Where(predicate).ToList();
        }

        public void Create(Tag item)
        {
            _context.Tags.Add(item);
        }

        public void Update(Tag item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var tag = _context.Tags.Find(id);
            if (tag != null)
                _context.Tags.Remove(tag);
        }
    }
}
