﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using WebApplication.DAL.Entities;

namespace WebApplication.DAL.EF
{
    /// <summary>
    /// Initialize library with severals data if database not exists. 
    /// </summary>
    public class LibraryDBInitializer : CreateDatabaseIfNotExists<LibraryContext>
    {
        protected override void Seed(LibraryContext context)
        {
            var tags = new List<Tag>
            {
                new Tag {Text = "Education"},
                new Tag {Text = "Policy"},
                new Tag {Text = "History"},
                new Tag {Text = "Builde"},
                new Tag {Text = "Health"},
                new Tag {Text = "Family"},
                new Tag {Text = "Bullying"}
            };

            var articles = new List<Article>
            {
                new Article
                {
                    Title = "Чому вчителів не влаштовує дистанційне навчання, та що з цим робити", Content =
                        "Дистанційне навчання стало червоною ганчіркою для багатьох. Воно критикувалося як вчителями, так і батьками." +
                        "Останні навіть писали петицію щодо його заборони. У минулій публікації на запрошення \"Української правди. Життя\"" +
                        "освітні експерти аналізували негативне ставлення до дистанційного навчання з боку батьків. " +
                        "Цього разу експерти в галузі освіти обговорили ставлення до дистанційної освіти вчителів.",
                    Time = new DateTime(2020, 10, 5, 9, 13, 25),
                    Tags = new List<Tag>
                    {
                        tags[0]
                    }
                },
                new Article
                {
                    Title = "Трамп, Байден та Covid: як змінюються перегони в США через хворобу президента", Content =
                        "Звістка про те, що президент Трамп інфікований на Covid, стала топновиною останніх днів не тільки у США." +
                        "Хвороба очільника найпотужнішої держави світу, який до того ж входить до групи ризику – непересічна подія сама по собі." +
                        "Але ще важливішою її робить те, що президентська кампанія в США вже перебуває на фінішній прямій: до дня виборів залишається" +
                        "рівно місяць. Тож виникає багато запитань щодо того, як хвороба Трампа – за різних варіантів її розвитку – вплине на перегони" +
                        "та й загалом на політичну ситуацію в США. Не можна сказати, що такий перебіг подій є зовсім неочікуваним – зважаючи на масштаби" +
                        "епідемії, американські експерти аналізували таку ймовірну ситуацію. Та все одно новина мала ефект бомби, що вибухнула." +
                        "Хтось передусім висловлює співчуття Трампу.Інші звертають увагу на те, що Трамп несе особисту відповідальність за те, що " +
                        "сталося з ним та рештою країни через Covid.Адже саме він оголошував епідемію фейком, фантазією опозиції, виступав проти" +
                        "носіння масок тощо. Нездатність та небажання Білого дому ефективно координувати протидію епідемії у федеральному масштабі – це + також реальність.",
                    Time = new DateTime(2020, 10, 2, 8, 48, 36),
                    Tags = new List<Tag>
                    {
                        tags[1],
                        tags[4]
                    }
                },

                new Article
                {
                    Title =
                        "Звукорежисер Максим Демиденко: Наша мета – перетворити Бабин Яр з місця забуття в місце пам'яті",
                    Content =
                        "Аудіовізуальна інсталяція, відкрита в 79-річницю трагедії в Бабиному Яру, лаконічна і дуже емоційна по сприйняттю." +
                        "Поєднання візуального та звукового компонентів дають ефект незримого присутності жертв цієї трагедії." +
                        "Створенням цієї інсталяції займався звукорежисер Максим Демиденко, який має вагомий досвід у проектах як в Україні, так і" +
                        "за кордоном. Він розповів в інтерв'ю \"Українській правді. Життя\" про унікальний органі в цій інсталяції, \"Древо життя\" з" +
                        "10 колон на простреленою дзеркальному диску і свої відчуття від роботи над проектом. - Коли вас залучили до цього проекту, яке завдання" +
                        "перед вами ставили? - Ідея Іллі (Хржановський, художній керівник Меморіального центру Бабин Яр - ред.), Коли його тільки запросили" +
                        "брати участь в розробці концепції меморіального комплексу \"Бабин яр\", полягала в тому, щоб не чекати, поки комплекс відкриється," +
                        "а почати запускати проекти зараз. Щоб вводити цю територію, людей в контекст.",
                    Time = new DateTime(2020, 10, 5, 13, 10, 35),
                    Tags = new List<Tag>
                    {
                        tags[2],
                        tags[3]
                    }
                },

                new Article
                {
                    Title =
                        "Не станеш, поки не доїсиш\": як популярні фрази батьків впливають на розвиток дитини",
                    Content =
                        "Мова йде про фрази-паразити, які батьки часто використовують під час діалогу з дітьми. " +
                        "Вони вже встигли врости у нашу модель спілкування, як щось звичне і побутове. " +
                        "Батьки віддають перевагу саме таким коротким поясненням, бо вважають їх лаконічними та зрозумілими. " +
                        "Проте хід дитячих думок відрізняється від дорослого. Тому малеча, яка чує такі меседжі вперше, знаходить в них інші послання. " +
                        "Часто – дуже негативні.\"Українська правда. Життя\" поспілкувалася з цього приводу із сімейними " +
                        "психологинями Катериною Гольцберг та Світланою Шумською. " +
                        "Разом з експертами ми проаналізували перелік популярних батьківських фраз та їхній вплив на дитячу психіку. " +
                        "У цьому матеріалі ви дізнаєтесь: Які послання діти чують у батьківських висловлюваннях? " +
                        "Як безпечно заспокоїти малечу? Чому не можна прирівнювати дитину  із \"сином подруги\" та " +
                        "\"віддавати поліції\"? Як позбутися фраз - паразитів?",
                    Time = new DateTime(2002, 10, 24, 11,20, 34),
                    Tags = new List<Tag>
                    {
                        tags[5],
                        tags[6]
                    }
                }
             };

            var feedbacks = new List<Feedback>
            {
                new Feedback
                {
                    Content = "Найкраща стаття, яку я коли-небудь читав.", Name = "Oleksandr Starodub",
                    Time = new DateTime(2020, 10, 4, 9, 23, 43)
                },

                new Feedback
                {
                    Content = "Повністю погоджуюсь.", Name = "Ivan Kihtan",
                    Time = new DateTime(2020, 10, 1, 18, 23, 45)
                },

                new Feedback
                {
                    Content = "Дійсно, воно так і працює.", Name = "Ivan Freyuk",
                    Time = new DateTime(2020, 10, 5, 13, 46, 27)
                },

                new Feedback
                {
                    Content =  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua." +
                               "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat." +
                               "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur." +
                               "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                    Name = "Serhii Yurko", Time = new DateTime(2020, 10, 2, 15, 32, 53)
                }
            };

            var networks = new List<Network>
            {
                new Network {Name = "Telegram"},
                new Network {Name = "Viber"},
                new Network {Name = "Instagram"},
                new Network {Name = "Whatsapp"},
                new Network {Name = "Facebook"}
            };

            foreach (var network in networks) context.Networks.Add(network);
            foreach (var article in articles) context.Articles.Add(article);
            foreach (var feedback in feedbacks) context.Feedbacks.Add(feedback);
            foreach (var tag in tags) context.Tags.Add(tag);
            base.Seed(context);
        }
    }
}