﻿using System.Data.Entity;
using WebApplication.DAL.Entities;

namespace WebApplication.DAL.EF
{
    /// <summary>
    /// Context for saving data from Library database.
    /// </summary>
    public class LibraryContext : DbContext
    {
        static LibraryContext()
        {
            Database.SetInitializer(new LibraryDBInitializer());
        }

        public LibraryContext() : base("DBConnection"){}
        public LibraryContext(string connectionString) : base(connectionString){}
        public DbSet<Article> Articles { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<Network> Networks { get; set; }
        public DbSet<Questionnaire> Questionnaires { get; set; }
        public DbSet<Tag> Tags { get; set; }

        /// <summary>
        /// Create many-many relations between Article and Tag models. 
        /// </summary>
        /// <param name="modelBuilder">
        /// Builder for creating table ArticleTag.
        /// </param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Article>().HasMany(c => c.Tags)
                .WithMany(s => s.Articles)
                .Map(t => t.MapLeftKey("ArticleId")
                    .MapRightKey("TagId")
                    .ToTable("ArticleTag"));
        }
    }
}