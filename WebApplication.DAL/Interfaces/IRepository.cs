﻿using System;
using System.Collections.Generic;

namespace WebApplication.DAL.Interfaces
{
    /// <summary>
    /// Interface with all CRUD function for model.
    /// </summary>
    /// <typeparam name="T">
    /// The model from the database.
    /// </typeparam>
    public interface IRepository<T> where T: class
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        IEnumerable<T> Find(Func<T, Boolean> predicate);
        void Create(T item);
        void Update(T item);
        void Delete(int id);
    }
}
