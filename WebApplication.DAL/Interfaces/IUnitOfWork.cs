﻿using System;
using WebApplication.DAL.Entities;

namespace WebApplication.DAL.Interfaces
{
    public interface IUnitOfWork: IDisposable
    {
        IRepository<Article> Articles{ get; }
        IRepository<Feedback> Feedbacks { get; }
        IRepository<Network> Networks { get; }
        IRepository<Questionnaire> Questionnaire { get; }
        IRepository<Tag> Tags { get; }
        void Save();
    }
}
