﻿using System.Linq;
using System.Web.Mvc;
using WebApplication.BLL.Interfaces;
using WebApplication.WEB.Models;

namespace WebApplication.Controllers
{
    /// <summary>
    /// Controller for showing and sending users feedback.
    /// /// </summary>
    public class GuestController : Controller
    {
        /// <summary>
        /// Service for working with user feedback.
        /// </summary>
        private readonly IFeedbackService _feedbacksService;

        public GuestController(IFeedbackService service)
        {
            _feedbacksService = service;
        }

        /// <summary>
        /// The start view for users feedback.
        /// </summary>
        /// <returns>
        /// View with all feedback and feedback form.
        /// </returns>
        public ActionResult Index(int page = 1)
        {
            int pageSize = 3;
            var feedbackPerPages = _feedbacksService.GetFeedbacks().Skip((page - 1) * pageSize).Take(pageSize);
            var pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = _feedbacksService.GetFeedbacks().Count };
            var ivm = new FeedbackViewModel { PageInfo = pageInfo, Feedback = feedbackPerPages.ToList() };
            return View(ivm);
        }

        /// <summary>
        /// Check if the feedback is valid and save it in database.
        /// </summary>
        /// <param name="feedback">
        /// User feedback from the form. 
        /// </param>
        /// <returns>
        /// View with new feedback. Otherwise show client error massage.
        /// </returns>
        [HttpPost]
        public ActionResult SendFeedback(FeedbackViewModel feedback)
        {
            if (ModelState.IsValid)
            {
                _feedbacksService.AddFeedback(feedback.FeedbackObject);
                return RedirectToAction("Index");
            }

            int pageSize = 3;
            int page = 1;
            feedback.Feedback = _feedbacksService.GetFeedbacks();
            feedback.PageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = _feedbacksService.GetFeedbacks().Count };
            return View("Index", feedback);
        }
    }
}