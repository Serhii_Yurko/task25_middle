﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebApplication.BLL.DTO;
using WebApplication.BLL.Interfaces;
using WebApplication.WEB.Models;

namespace WebApplication.Controllers
{
    /// <summary>
    /// Controller for showing start page with articles.
    /// </summary>
    public class HomeController : Controller
    {
        private readonly IArticleService _articleService;
        private readonly ITagService _tagService;

        public HomeController(IArticleService articleService, ITagService tagService)
        {
            _articleService = articleService;
            _tagService = tagService;
        }

        /// <summary>
        /// Convert <c>TagDTO</c> list into <c>SelectListItem</c> list.
        /// </summary>
        /// <param name="list">List for converting.</param>
        /// <returns>New SelectListItem converted list.</returns>
        private List<SelectListItem> getSelectList(List<TagDTO> list)
        {
            var items = new List<SelectListItem>();
            list.ForEach(tag => items.Add(new SelectListItem { Text = tag.Text, Value = tag.Id.ToString()}));
            return items;
        }

        /// <summary>
        /// Start page with articles.
        /// </summary>
        /// <returns>View with all info about articles.</returns>
        public ActionResult Index(int page = 1)
        {
            int pageSize = 3;
            var articlesPerPages = _articleService.GetAllArticles().Skip((page - 1) * pageSize).Take(pageSize);
            var pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = _articleService.GetAllArticles().Count };
            var ivm = new ArticlesViewModel { PageInfo = pageInfo, Articles = articlesPerPages.ToList() };
            return View(ivm);
        }

        /// <summary>
        /// Display full info about article.
        /// </summary>
        /// <param name="id">Selected article.</param>
        /// <returns>View with full info about article.</returns>
        public ActionResult GetFullArticle(int? id)
        {
            if (id != null)
            {
                var article = _articleService.GetArticle((int)id);
                return View(article);
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Display all articles filtered by tag id.
        /// </summary>
        /// <param name="id">Tag id for filtering.</param>
        /// <param name="page">Number of the page</param>
        /// <returns>View with all filtered articles.</returns>
        public ActionResult GetAllArticlesByTag(int id, int page = 1)
        {
            int pageSize = 3;
            var articlesPerPages = _articleService.GetAllArticles(id).Skip((page - 1) * pageSize).Take(pageSize);
            var pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = _articleService.GetAllArticles(id).Count };
            var ivm = new ArticlesViewModel { PageInfo = pageInfo, Articles = articlesPerPages.ToList() };
            return View("Index", ivm);
        }

        /// <summary>
        /// Display adding tags to the article. 
        /// </summary>
        /// <param name="articleId">Article for changing.</param>
        /// <returns>Main page.</returns>
        public ActionResult AddTags(int? articleId)
        {
            if (articleId != null)
            {
                var article = _articleService.GetArticle((int)articleId);
                var tags = _tagService.GetTags();
                return View(new ArticleTagsViewModel { Article = article, Tags = getSelectList(tags) });
            }

            return RedirectToAction("Index");
        }


        /// <summary>
        /// Add new tags to the article.
        /// </summary>
        /// <param name="articleTags">Model for adding.</param>
        /// <returns>Main page.</returns>
        [HttpPost]
        public ActionResult AddTags(ArticleTagsViewModel articleTags)
        {
            _articleService.AddTags(articleTags.Article.Id, articleTags.SelectedTags);

            if(!string.IsNullOrEmpty(articleTags.InputTag))
                _articleService.AddTag(articleTags.Article.Id, articleTags.InputTag);
            
            return RedirectToAction("Index");
        }
    }
}