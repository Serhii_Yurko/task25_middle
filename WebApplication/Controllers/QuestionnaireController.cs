﻿using System.Collections.Generic;
using System.Web.Mvc;
using WebApplication.BLL.DTO;
using WebApplication.BLL.Interfaces;
using WebApplication.WEB.Models;

namespace WebApplication.Controllers
{
    /// <summary>
    /// Controller for working with user questionnaire.
    /// </summary>
    public class QuestionnaireController : Controller
    {
        private readonly IQuestionnaireService _questionnaireService;

        public QuestionnaireController(IQuestionnaireService service)
        {
            _questionnaireService = service;
        }

        /// <summary>
        /// Show view for questionnaire.
        /// </summary>
        /// <returns>
        /// View for filling in the data in the questionnaire.
        /// </returns>
        [HttpGet]
        public ActionResult QuestionnaireRegister()
        {
            return View(new QuestionnaireViewModel());
        }

        /// <summary>
        /// Show the result of questionnaire.
        /// </summary>
        /// <param name="questionnaire">
        /// Object from View with inputed data.
        /// </param>
        /// <returns>
        /// View with the filling data.
        /// </returns>
        [HttpPost]
        public ActionResult QuestionnaireRegister(QuestionnaireViewModel questionnaire)
        {
            if (string.IsNullOrEmpty(questionnaire.FName))
                ModelState.AddModelError("FName", "Please enter your first name");

            if (string.IsNullOrEmpty(questionnaire.LName))
                ModelState.AddModelError("LName", "Please enter your last name");

            if (!questionnaire.IsConfirmed)
                ModelState.AddModelError("IsConfirmed", "You must accept the terms");

            if (ModelState.IsValid)
            {
                ViewBag.FirstName = questionnaire.FName;
                ViewBag.LastName = questionnaire.LName;
                ViewBag.Sex = questionnaire.Sex;
                List<NetworkDTO> socailNetworkLists = new List<NetworkDTO>();
                for (int i = 0; i < questionnaire.Networks.Count; i++)
                {
                    if (questionnaire.Networks[i].IsSelected)
                        socailNetworkLists.Add(new NetworkDTO { Name = questionnaire.Networks[i].Name });
                }
                ViewBag.SocailNetworkLists = socailNetworkLists;
                ViewBag.Mark = questionnaire.Mark;
                _questionnaireService.AddQuestionnaire(questionnaire.FName, questionnaire.FName, questionnaire.Sex,
                    questionnaire.Mark, socailNetworkLists);
                return View("QuestionnaireCompleted");
            }

            return View(new QuestionnaireViewModel());
        }

    }
}