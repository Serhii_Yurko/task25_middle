﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace WebApplication.Helpers
{
    public static class ListHelpers
    {
        /// <summary>
        /// Helper for representing articles on the page.
        /// </summary>
        /// <param name="helper">This helper</param>
        /// <param name="articleTitle">All articles title.</param>
        /// <param name="date">Dates of published articles</param>
        /// <param name="articleContent">Description of all articles</param>
        /// <returns>Block with all articles</returns>
        public static MvcHtmlString CreateArticle(this HtmlHelper helper, string articleTitle, DateTime date,
            string articleContent, int charCount = 200)
        {
            TagBuilder content = new TagBuilder("div");

            TagBuilder div = new TagBuilder("div");
            div.AddCssClass("article");
            TagBuilder h2 = new TagBuilder("h2");
            TagBuilder h3 = new TagBuilder("h3");
            TagBuilder p = new TagBuilder("p");
            h2.AddCssClass("article_title");
            h3.AddCssClass("article_date");
            p.AddCssClass("article_content");
            h2.SetInnerText(articleTitle);
            h3.SetInnerText(date.ToLongDateString());

            string text = "";
            var articleWords = articleContent.Split(' ');
            var articleChars = articleWords.Select(i => i.ToCharArray()).ToList();
            if (articleChars.Sum(word => word.Length) <= charCount)
                p.SetInnerText(articleContent.Aggregate(text, (current, t) => current + t));
            else
            {
                for (int i = 0; i < charCount; i++)
                    text += articleContent[i];
                text += "...";
                p.SetInnerText(text);
            }
            div.InnerHtml += h2.ToString();
            div.InnerHtml += h3.ToString();
            div.InnerHtml += p.ToString();
            content.InnerHtml += div;
            return new MvcHtmlString(content.ToString());
        }

        /// <summary>
        /// Helper for representing all feedback on the page.
        /// </summary>
        /// <param name="helper">This helper</param>
        /// <param name="name">The user names</param>
        /// <param name="date">Dates of published all feedback</param>
        /// <param name="contentText">Contents of published all feedback</param>
        /// <returns>
        /// Block with all feedback.
        /// </returns>
        public static MvcHtmlString CreateFeedback(this HtmlHelper helper, string name, DateTime date,
            string contentText)
        {
            TagBuilder content = new TagBuilder("div");

            TagBuilder div = new TagBuilder("div");
            div.MergeAttribute("class", "feedback");
            TagBuilder p1 = new TagBuilder("p");
            p1.SetInnerText(name);
            TagBuilder span = new TagBuilder("span");
            span.SetInnerText(date.Day + "/" + date.Month + "/" + date.Year + " " + date.Hour + ":" + date.Minute);
            TagBuilder p2 = new TagBuilder("p");
            p2.SetInnerText(contentText);
            div.InnerHtml += p1;
            div.InnerHtml += span;
            div.InnerHtml += p2;
            content.InnerHtml += div;
            return new MvcHtmlString(content.ToString());
        }
    }
}